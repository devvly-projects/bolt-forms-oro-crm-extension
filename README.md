ORO CRM Leads Feed
======================
##### Description:
This extension is used to generate leads out of forms for ORO CRM.
##### Usage:
The extension's configuration file is `/app/config/extensions/oclf.devvly.yml`
1. Edit the required api fields: <br>
   1. `oro_app_url`: The CRM url, e.g. http://example.com
   1. `oro_app_user`: Your username
   1. `oro_app_user_key`: the generated api key, check <a href="https://oroinc.com/orocrm/doc/2.0/dev-guide/web-api#create-an-api-key">this url</a> on how to do this
 
2. Add any form as usual using Boltforms extension in `/app/config/extensions/boltforms.bolt.yml`.
3. edit the file `/app/config/extensions/oclf.devvly.yml` to include this form, for example:
     ~~~~ 
     forms:
       0:
         name: "contact"
         redirect_to: "/"
     ~~~~ 
4. start mapping the form fields:
   let's suppose you want to generate a lead from the submitted contact form, the lead configuration will be:
   
   ~~~
   lead:
     owner:
       entity_type: "users"
       value: "admin"
     name: "$name"
     status: "new"
     companyName: null
     notes: null
     namePrefix: null
     firstName:
       value: "$name"
       plugins:
         explode:
           parameters:
             - "$value"
             - " "
         first:
     middleName: null
     lastName:
       value: "$name"
       plugins:
         explode:
           parameters:
             - "$value"
             - " "
         last:
     jobTitle: null
     primaryEmail: "$email"
     primaryPhone: "585-255-1127"
     contact: null
   
     account: null
     twitter: null
     linkeden: null
     addresses:
       multiple: true
       0:
         primary: true
         label: "$name"
         namePrefix: null
         firstName:
           value: "$name"
           plugins:
             explode:
               parameters:
                 - "$value"
                 - " "
             first:
         middleName: null
         lastName:
           value: "$name"
           plugins:
             explode:
               parameters:
                 - "$value"
                 - " "
             last:
   
         organization: null
         country: "US"
         region: "US-AK"
         street: "abc"
         street2: "abc"
         city: "test"
         postalCode: "000000"
   ~~~
5. Display the form: <br>
   in the template where you want to display the form, use `{{ oclf("contact") }}` <br>
   _Note: this function extends the `boltforms()` function, so you can pass additional configuration to it._
##### Additional Configurations
1. **Entity field:** <br>
   Sometimes a lead field's value should be an entity id, and we don't know the id, to get this entity id, we used this configuration:
   ~~~
   owner:
     entity_type: "users"
     field: "username"
     value: "admin"
   ~~~
   this tells the extension to query an entity called `users` and filter on `username` field where it's equal to `admin`

1. **Field value reference:** <br>
   notice how we used the value `$name` for `lead.name` field, this tells the extension to map the value of the contact form's field `name`, to the lead's field `name`:
   ~~~
   [lead_field]: [form field name] 
   ~~~
1. **Multi-valued field:** <br>
    we used the configuration `multiple: true` on the `addresses` field, this tells the extension that this field can hold multiple values
1. **Plugins:** <br>
    when you want to split a form field value on multiple lead fields, use plugins, <br> 
    for example, suppose you have `full_name` field in the form, and you want to map its value to the lead's `firstName` and `lastName` fields, <br>
    in this case we use the plugins `explode` and `first`:
    ~~~
      firstName:
        value: "$name"
        plugins:
          explode:
            parameters:
              - "$value"
              - " "
          first:   
    ~~~
    plugins are nothing more than simple php functions, defined in: <br>
    `/extensions/vendor/devvly/oclf/src/Service/PluginService.php` <br>
    as you can see the parameters for the `explode` function are `$value` and `$delimiter`, <br>
    and we used: 
    ~~~
    parameters:
      - "$value"
      - " "
    ~~~
    as configuration for it, to pass the form field's value as the first parameter, and "space" for the second parameter, <br>
    if we had `full_name` value as "John Doe", the output of this plugin with this configuration will be:
    ~~~
    array(2) {
      [0]=>
      string(4) "John"
      [1]=>
      string(3) "Doe"
    }
    ~~~
    then to take the first value (John), we use a second plugin called `first` with no configuration,
    because it has only one parameter `$value`, and by default, the previous plugin's output (explode) is automatically being passed to the next plugin (first).
    
    