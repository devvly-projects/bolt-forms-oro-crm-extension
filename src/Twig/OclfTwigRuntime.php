<?php

namespace Bolt\Extension\Devvly\Oclf\Twig;


use Bolt\Extension\Devvly\Oclf\Service\ConfigService;
use Silex\Application;

class OclfTwigRuntime
{

    /**
     * @var \Silex\Application
     */
    private $app;

    /**
     * @var ConfigService
     */
    private $config;

    public function __construct(ConfigService $config, Application $app)
    {

        $this->app = $app;
        $this->config = $config;
    }


    public function buildForm(
      $formName,
      $htmlPreSubmit = null,
      $htmlPostSubmit = null,
      $data = null,
      $options = [],
      $defaults = null,
      $overrides = null,
      $meta = null,
      $action = null
    ){
        /** @var \Bolt\Extension\Bolt\BoltForms\Twig\Extension\BoltFormsRuntime $boltForms*/
        $boltForms = $this->app['twig.runtime.boltforms'];
        $twig = $this->app['twig'];
        $form = $this->config->getForm($formName);
        if(!$form){
            return $boltForms->twigBoltForms(
              $twig,
              $formName,
              $htmlPreSubmit,
              $htmlPostSubmit,
              $data,
              $options,
              $defaults,
              $overrides,
              $meta,
              $action
            );
        }
        $defaultOverrides = [
         'feedback' => [
           'redirect' => [
             'target' => $form['redirect_to']
           ],
         ],
        ];
        // TODO: enable notification on production
        $defaultOverrides['notification'] = ['enabled' => false];

        $finalOverrides = $defaultOverrides;
        if($overrides !== null){
            foreach ($overrides as $key => $value) {
                if (isset($finalOverrides['fields'][$key])) {
                    $finalOverrides['fields'][$key] = $value;
                } else {
                    $finalOverrides[$key] = $value;
                }
            }
        }
        return $boltForms->twigBoltForms(
          $twig,
          $formName,
          $htmlPreSubmit,
          $htmlPostSubmit,
          $data,
          $options,
          $defaults,
          $finalOverrides,
          $meta,
          $action
        );
    }

}