<?php

namespace Bolt\Extension\Devvly\Oclf\Service;

use Bolt\Application;
use Bolt\Storage\Entity\Entity;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
class LeadService
{

    /**
     * @var ConfigService
     */
    private $config;

    /**
     * @var \Bolt\Application
     */
    private $app;

    public function __construct(ConfigService $config, Application $app)
    {
        $this->config = $config;
        $this->app = $app;
    }

    /**
     * @return \Bolt\Application
     */
    public function getApp()
    {
        return $this->app;
    }
    public function submitLead(Entity $formData){
        // TODO: get the form file to redirect to it:
        $lead = $this->overrideValues($formData);
        $valid = $this->config->isValid($lead);
        if(!$valid){
            return;
        }
        // adding emails or phones collection directly to the lead causing problem, remove them,
        // and use instead the primaryPhone & primaryEmail properties:
        if(isset($lead['emails'])){
            unset($lead['emails']);
        }
        if(isset($lead['phones'])){
            unset($lead['phones']);
        }
        // Remove address from the lead to create it later after the lead:
        $addresses = [];
        if(isset($lead['addresses']) && !empty($lead['addresses'])){
            $addresses = $lead['addresses'];
            unset($lead['addresses']);
        }
        $result = $this->makeRequest('/leads','POST',$lead);

        // If not saved, return null:
        $saved = isset($result['status']) && $result['status'] === 'success';
        if(!$saved){
            return;
        }

        // set one of the addresses as primary:
        $lead = $result['response'];
        $hasPrimary = array_filter($addresses,function ($address){
            return isset($address['primary']) && $address['primary'] === true;
        });
        if(!count($hasPrimary)){
            $addresses[0]['primary'] = true;
        }

        // attach address:
        foreach ($addresses as $key => $address) {
            $address['owner'] = $lead['id'];
            $result = $this->makeRequest('/leadaddresses','POST', $address);
            if(isset($result['status']) && $result['status'] === 'success'){
                $lead['addresses'][$key] = [
                  'id' => $result['response']['id']
                ];
            }
        }
    }

    protected function overrideValues(Entity $formData){
        // Get the lead fields defined in config.yml
        $config = $this->config->getConfig();
        $leadFields = array_keys($config['lead']);
        $values = [];
        foreach ($leadFields as $field) {
            $value = $this->config->findConfigValue($field);
            $finalValue = null;
            // if the field is entity field, query the entity:
            if(is_array($value)){
                $isValid = $this->config->validateEntityFieldConfig($value);
                if($isValid){
                    $finalValue = $this->filterEntity(
                      $value['entity_type'],
                      $value['field'],
                      $value['value'],
                      $value['references']);
                }
            }
            if($finalValue === null){
                $finalValue = $value;
            }
            $finalValue = $this->config->mapConfigValue($field, $finalValue, $formData->_fields);
            // only include the field if has value:
            if($finalValue !== null && !empty($finalValue)){
                $values[$field] = $finalValue;
            }
        }
        return $values;
    }

    /**
     * Gets an entity id by a value
     * @param string $type
     * @param string $field
     * @param mixed $value
     * @param string $reference
     *
     * @return mixed|null
     */
    protected function filterEntity($type, $field, $value, $reference){
        $parameters = [
          'filter' => [
            $field => $value,
          ]
        ];
        $result = $this->makeRequest('/' . $type, 'GET',null,$parameters);
        if(isset($result['status']) && $result['status'] === 'success'){
            $response = reset($result['response']);
            return $response[$reference];
        }
        return [];
    }

    protected function getUser($username){
        $parameters = [
          'filter' => [
            'username' => $username,
          ]
        ];
        $result = $this->makeRequest('/users', 'GET',null,$parameters);
        if(isset($result['status']) && $result['status'] === 'success'){
            return reset($result['response']);
        }
        return [];
    }


    /**
     * @param string $url
     * @param  string $method
     * @param  array $body
     * @param  array $parameters
     * @param  array $headers
     *
     * @return array
     */
    protected function makeRequest($url, $method = 'GET', $body = null, $parameters = null, $headers = null){
        /** @var Client $client */
        $client = $this->app['guzzle.client'];
        $config = $this->config->getConfig();
        $url = $config['oro_app_url'] . '/api' . $url;

        $options = [
          'headers' => $this->generateAuthHeaders(),
        ];
        $result = [];
        if($body !== null){
            $options['form_params'] = $body;
        }
        if($parameters !== null){
            $options['query'] = $parameters;
        }
        if($headers !== null){
            $options['headers'] = array_merge($options['headers'], $headers);
        }
        try {
            $resp = $client->request($method, $url, $options);
            $content = $resp->getBody()->getContents();
            $content = json_decode($content,TRUE);
            $result['status'] = 'success';
            $result['response'] = $content;
        } catch (GuzzleException $e) {
            $response = $e->getResponse();
            $content = null;
            if($response){
                $content = $response->getBody()->getContents();
            }
            if(is_string($content) && !strlen($content)){
                $content = $e->getMessage();
            }
            $result['status'] = 'error';
            $result['response'] = $content;
            /** @var \Monolog\Logger $logger */
            $logger = $this->app['logger.system'];
            $message = 'Failed to create a lead entry';
            $context = [
              'event' => 'exception',
              'error' => $e->getMessage(),
              'content' => $content
            ];
            $logger->warning($message,$context);
        }

        return $result;
    }

    protected function generateAuthHeaders(){
        $config = $this->config->getConfig();
        $userName = $config['oro_app_user'];
        $userApiKey = $config['oro_app_user_key'];
        $nonce = base64_encode(substr(md5(uniqid()), 0, 16));
        $created  = date('c');
        $digest   = base64_encode(sha1(base64_decode($nonce) . $created . $userApiKey, true));

        $authHeader = 'WSSE profile="UsernameToken"';
        $wsseHeader = sprintf(
          'UsernameToken Username="%s", PasswordDigest="%s", Nonce="%s", Created="%s"',
          $userName,
          $digest,
          $nonce,
          $created
        );
        return [
          'Authorization' => $authHeader,
          'X-WSSE' => $wsseHeader,
        ];
    }


}