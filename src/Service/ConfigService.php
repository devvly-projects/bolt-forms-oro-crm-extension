<?php


namespace Bolt\Extension\Devvly\Oclf\Service;


use Bolt\Application;

class ConfigService
{

    /**
     * @var array
     */
    private $config;

    /**
     * @var \Bolt\Application
     */
    private $app;

    /**
     * @var \Bolt\Extension\Devvly\Oclf\Service\PluginService
     */
    private $pluginService;

    public function __construct(array $config, PluginService $pluginService, Application $app)
    {
        $this->config = $config;
        $this->app = $app;
        $this->pluginService = $pluginService;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param  array  $values
     *
     * @return bool
     */
    public function isValid(array $values){
        $required = [
          'owner',
          'name',
          'status'
        ];
        foreach ($required as $item) {
            if(!isset($values[$item])){
                return false;
            }
        }
        return true;
    }


    public function validateEntityFieldConfig(array $field){
        // Validate entity_type:
        if(!isset($field['entity_type']) || empty($field['entity_type'])){
            return false;
        }
        // Validate field:
        if(!isset($field['field']) || empty($field['field'])){
            return false;
        }

        // Validate value
        if(!isset($field['value']) || empty($field['value'])){
            return false;
        }

        // Validate references
        if(!isset($field['references']) || empty($field['references'])){
            return false;
        }
        return true;
    }

    /**
     * finds form by name.
     * @param $name
     *
     * @return array|null
     */
    public function getForm($name){
        $config = $this->getConfig();
        if(!isset($config['forms'])){
            return null;
        }
        $form = array_filter($config['forms'],function ($form) use ($name){
            if(isset($form['name'])){
                return $form['name'] === $name;
            }
            return false;
        });
        if(count($form) > 0){
            $form = reset($form);
            if(!isset($form['redirect_to']) || empty($form['redirect_to'])){
                return null;
            }
            return $form;
        }
        return null;
    }
    public function mapConfigValue($field, $value, array $formData, $parent = null){
        if(is_array($value) && !isset($value['value'])){
            $values = [];
            foreach ($value as $key => $item) {
                if(is_string($key)){
                    $values[$key] = $this->mapConfigValue($key, $item, $formData, $field);
                }else{
                    $values[$key] = $this->mapConfigValue($field, $item, $formData, $parent);
                }
            }
            return $values;
        }
        return $this->setFieldValue($field, $value, $formData, $parent);
    }

    protected function setFieldValue($field, $value, $formData, $parentField = null){
        $cleanVal = $value;
        if(is_array($value) && isset($value['value'])){
            $cleanVal = $value['value'];
        }
        // check whether the value is referencing a form field value:
        if(!$this->isReference($cleanVal)){
            return $value;
        }
        $cleanVal = $this->getReferenceValue($cleanVal, $formData);
        if($parentField && $parentField !== $field){
            $parent = $this->findConfigField($parentField);
            $field = $this->findConfigField($field, $parent['value']);
        }else{
            $field = $this->findConfigField($field);
        }
        // if there are plugins, run them:
        if($this->pluginService->hasPlugins($field)){
            $plugins = $field['value']['plugins'];
            $pluginVal = $cleanVal;
            foreach ($plugins as $name => $config) {
                $pluginVal = $this->pluginService->runPlugin($name, $pluginVal, $config);
            }
            return $pluginVal;
        }
        return $cleanVal;
    }

    protected function getReferenceValue($value, array $formData){
        $ref = str_replace('$','',$value);
        if(!isset($formData[$ref])){
            return null;
        }
        return $formData[$ref];
    }
    protected function findConfigField($name, $configFields = null, $queries = []){
        $originalConfig = $this->getConfig();
        if($configFields === null){
            $configFields = $originalConfig;
        }
        foreach ($configFields as $configField => $value) {
            if($configField === $name){
                return [
                  'field' => $configField,
                  'value' => $value,
                ];
            }
            $isArray = is_array($value)
              && !isset($value['entity_type'])
              && !isset($value['multiple'])
              && !isset($value['value']);
            if($isArray){
                if(isset($queries[$name]) && $queries[$name] > 0){
                    return $configField;
                }else{
                    $queries[$name] = 1;
                }
                $res = $this->findConfigField($name, $value, $queries);
                if($res !== false){
                    return $res;
                }
            }
        }
        // the field not found, try searching on the original config fields:
        if($this->fieldExists($name)){
            return $this->findConfigField($name, $originalConfig, $queries);
        }
        return false;
    }

    public function findConfigValue($name){
        $field = $this->findConfigField($name);
        $value = $field['value'];
        //if the value is mixed with config:
        if(isset($value['value']) && !isset($value['entity_type'])){
            $value = $value['value'];
        }
        // if multi-valued field, hook into sub values:
        if(isset($value['multiple']) && $value['multiple']){
            $value = $this->getSubconfigValue($value);
        }
        return $value;
    }

    protected function getSubconfigValue(array $fields, $result = [], $resKey = null){
        // The result of this function is just $fields without "multiple" key
        // the purpose of this is to hook into sub config fields later:
        if(isset($fields['multiple'])){
            unset($fields['multiple']);
        }
        return $fields;
    }

    protected function isReference($value){
        return is_string($value) && strpos($value,'$') !== false;
    }

    protected function hasSubfield(array $field, $name){
        foreach ($field as $subField) {
            if(is_array($subField)){
                $exists = key_exists($name, $subField);
                if($exists){
                    return true;
                }else{
                    return $this->hasSubfield($subField, $name);
                }
            }
        }
        return key_exists($name, $field);
    }

    /**
     * Checks whether a configuration field exists.
     *
     * @param string $name the field name
     * @param  array $fields config subject to search the field for
     *
     * @return bool
     */
    public function fieldExists($name, $fields = null){
        $configFields = $this->getConfig();
        if($fields !== null){
            $configFields = $fields;
        }
        foreach ($configFields as $configField => $value) {
            if(is_array($value) && !isset($value['content_type'])){
                $exists = $this->fieldExists($name, $value);
                if($exists){
                    return true;
                }
            }
            if($configField !== $name){
                continue;
            }
            // if the field value is a configuration,
            // get the value from the config:
            if(is_array($value) && isset($value['value'])){
                $value = $value['value'];
            }

            // If the field value is by reference,
            // get the referenced field value:
            if(is_string($value) && strpos($value,'$') !== false){
                $name = str_replace('$','',$value);
                $exists = $this->fieldExists($name,$configFields);
                if($exists){
                    return true;
                }
            }elseif(is_array($value)){
                $exists = $this->fieldExists($name, $value);
                if($exists){
                    return true;
                }
            }
            return true;
        }
        return false;
    }
}