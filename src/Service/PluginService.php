<?php


namespace Bolt\Extension\Devvly\Oclf\Service;


class PluginService
{

    public function hasPlugins(array $field){
        $hasPlugins = isset($field['value']['plugins'])
          && is_array($field['value']['plugins'])
          && !empty($field['value']['plugins']);
        return $hasPlugins;
    }
    public function runPlugin($name, $value, $config = null){
        $methods = get_class_methods(PluginService::class);
        if(!in_array($name,$methods)){
            return $value;
        }
        foreach ($methods as $method) {
            if($method === $name){
                $parameters = $this->collectParameters($value, $config);
                $ref = new \ReflectionClass(PluginService::class);
                $method = $ref->getMethod($name);
                return $method->invokeArgs(new PluginService(), $parameters);
            }
        }

        return $value;
    }

    protected function collectParameters($value, $config = null){
        $parameters = [];
        $hasParameters = isset($config['parameters'])
          && is_array($config['parameters'])
          && !empty($config['parameters']);
        if($hasParameters){
            foreach ($config['parameters'] as $parameter) {
                $res = $this->getReference($parameter, $value);
                if($res !== null){
                    $parameters[] = $res;
                }elseif(!empty($parameter)){
                    $parameters[] = $parameter;
                }
            }
        }
        if(empty($parameters)){
            $parameters[] = $value;
        }
        return $parameters;
    }
    protected function getReference($ref, $value){
        $aliases = [
          'value' => $value,
        ];
        $ref = str_replace('$','',$ref);
        foreach ($aliases as $alias => $aliasVal) {
            if($alias == $ref){
                return $aliasVal;
            }
        }
        return null;
    }

    public static function explode($value, $delimiter = ','){
        if(!is_string($value)){
            return $value;
        }
        $res = explode($delimiter,$value);
        return $res;
    }

    public static function last($value){
        if(!is_array($value)){
            return $value;
        }
        return end($value);
    }

    public static function first($value){
        if(!is_array($value)){
            return $value;
        }
        return reset($value);
    }
}