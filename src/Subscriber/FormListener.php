<?php
namespace Bolt\Extension\Devvly\Oclf\Subscriber;
use Bolt\Extension\Devvly\Oclf\Service;
use Bolt\Extension\Bolt\BoltForms\Event\LifecycleEvent;

class FormListener
{
    public function onRedirect($ev){
        // todo: edit this:
        $app = $GLOBALS['app'];
        /** @var Service\LeadService $leadService */
        $leadService = $app['oclf.lead'];
        $leadService->submitLead($ev->getData());
        //$oclf->submitLead($ev->getFormData());
    }
}