<?php

namespace Bolt\Extension\Devvly\Oclf\Controller;

use Bolt\Application;
use Bolt\Controller\Base;
use Bolt\Extension\Bolt\BoltForms\Submission\Handler\PostRequest;
use Symfony\Component\HttpFoundation\Request;
class FrontendFormController extends Base
{
    protected function addRoutes(\Silex\ControllerCollection $c)
    {

        $c->match('/submit/{formName}', [$this, 'submit'])
          ->method('POST');

        return $c;
    }

    /**
     * @param  \Symfony\Component\HttpFoundation\Request  $request
     * @param  \Bolt\Application  $app
     * @param  string $formName
     *
     * @return \Bolt\Response\TemplateResponse|\Bolt\Response\TemplateView
     */
    public function submit(Request $request, Application $app, $formName){
        $postRequest = new PostRequest($app['request_stack']);
        $postRequest->handle($formName);
    }
}