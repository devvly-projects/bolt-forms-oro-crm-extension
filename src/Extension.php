<?php

namespace Bolt\Extension\Devvly\Oclf;

use Bolt\Extension\Bolt\BoltForms\Event\BoltFormsEvents;
use Bolt\Extension\DatabaseSchemaTrait;
use Bolt\Extension\Devvly\Oclf\Controller\FrontendFormController;
use Bolt\Extension\Devvly\Oclf\Service\ConfigService;
use Bolt\Extension\Devvly\Oclf\Service\LeadService;
use Bolt\Extension\Devvly\Oclf\Service\PluginService;
use Bolt\Extension\Devvly\Oclf\Storage\Schema\Table\FieldTable;
use Bolt\Extension\Devvly\Oclf\Storage\Schema\Table\FormFieldTable;
use Bolt\Extension\Devvly\Oclf\Storage\Schema\Table\FormTable;
use Bolt\Extension\Devvly\Oclf\Subscriber\FormListener;
use Bolt\Extension\Devvly\Oclf\Twig;
use Bolt\Extension\SimpleExtension;
use Bolt\Extension\Devvly\Oclf\Controller\BackendFormController;
use Bolt\Menu\MenuEntry;
use Silex\Application;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


/**
 * ExtensionName extension class.
 *
 * @author Your Name <you@example.com>
 */
class Extension extends SimpleExtension
{
    use DatabaseSchemaTrait;

    protected function registerServices(Application $app)
    {
        $this->extendDatabaseSchemaServices();
        $this->extendRepositoryMapping();
        // Register app services:
         // Register app configuration service:
        // plugin service:
        $app['oclf.plugin'] = $app->share(
          function ($app) {
              return new PluginService();
          }
        );
        $app['oclf.config'] = $app->share(
          function ($app) {
              return new ConfigService($this->getConfig(),$app['oclf.plugin'], $app);
          }
        );
        // lead sevice:
        $app['oclf.lead'] = $app->share(
          function ($app) {
              return new LeadService($app['oclf.config'], $app);
          }
        );
        // register twig custom function provider:
        $app['twig.runtime.oclf'] = $app->share(function ($app) {
            return new Twig\OclfTwigRuntime($app['oclf.config'],$app);
        });
        $app['twig.runtimes'] = $app->extend(
          'twig.runtimes',
          function (array $runtimes) {
              // You must append your array to the passed in $runtimes array and return it
              return $runtimes + [
                  Twig\OclfTwigRuntime::class => 'twig.runtime.oclf',
                ];
          }
        );
        parent::registerServices($app);
    }

    protected function subscribe(EventDispatcherInterface $dispatcher)
    {
        // add a listener for when boltforms starts redirecting:
        $listener = new FormListener();
        // todo: change event to SUBMISSION_PROCESS_REDIRECT
        $dispatcher->addListener(BoltFormsEvents::SUBMISSION_PRE_PROCESSOR,[$listener,'onRedirect']);
        parent::subscribe($dispatcher);
    }

    protected function registerFrontendControllers()
    {
        return [
          '/oclf' => new FrontendFormController(),
        ];
    }

    protected function registerTwigPaths()
    {
        return [
          'templates' => ['namespace' => 'oclf'],
        ];
    }

    protected function registerTwigFunctions()
    {
        $callable = [Twig\OclfTwigRuntime::class, 'buildForm'];
        return [
          'oclf' => [$callable]
        ];
    }

    protected function getConfig()
    {
        return parent::getConfig();
    }

    protected function getDefaultConfig()
    {
        return [
          'default_file' => null,
          'oro_app_user' => null,
          'oro_app_user_key' => null,
          'oro_app_url' => null,
          'lead' => [
            'owner' => null,
            'name' => null,
            'status' => 'new',
          ]
        ];
    }

}
